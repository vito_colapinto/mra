package tdd.training.mra;

import java.util.*;

import static org.junit.Assert.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testCreatePlanetWithObstacles() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		boolean obstacle= rover.planetContainsObstacleAt(4, 7);
		
		assertTrue(obstacle);
	}

	@Test
	public void testLandingWithReturnPositionCommand() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		
		
		assertEquals("(0,0,N)",rover.executeCommand(""));
	}
	
	@Test
	public void testTurningRightShouldBeEst() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		
		
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	public void testTurningLeftShouldBeWest() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
	
	@Test
	public void testGoingForward() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	
	@Test
	public void testGoingBackward() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		rover.executeCommand("f");
		assertEquals("(0,0,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testGoingBackwardLookingEast() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.executeCommand("r");
		rover.executeCommand("f");
		assertEquals("(0,0,E)",rover.executeCommand("b"));
	}
	
	@Test
	public void testMovingCombined() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(2,2,E)",rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testPlanetWrapping() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,9,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testSingleObstacle() throws MarsRoverException{
		List<String> planetObstacles= new ArrayList<String>();
		
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String returnString=rover.executeCommand("ffrfff");
		
		assertEquals("(1,2,E)(2,2)",returnString);
	}
}
