package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	private int[][] planet;
	private int numOstacoli;
	private int roverx;
	private int rovery;
	private String dir;
	private int gridMaxX;
	private int gridMaxY;
	private String Obstacles="";
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		  planet=new int[planetX][planetY];
		  gridMaxX=planetX;
		  gridMaxY=planetY;
		  String ostacolo;
		  int oix;
		  int oiy;
		  
		  for(int i=0;i<planetX;i++) {
			  for(int j=0;j<planetY;j++) {
				  planet[i][j]=0;
			  }
		  }
		  
		  planet[0][0]=1; //"1" is the position of our rover "0" nothing and "2" are the obstacles 
		  roverx=0;
		  rovery=0;
		  dir="N";
		  numOstacoli=planetObstacles.size();
		  
		  for(int i=0;i<numOstacoli;i++) {
			  ostacolo=planetObstacles.get(i);
			  oix=Integer.parseInt(ostacolo.charAt(1)+"");
			  oiy=Integer.parseInt(ostacolo.charAt(3)+"");
			  planet[oix][oiy]=2;
		  }
		  
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {
		
		return planet[x][y]==2;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		char commands[];
		commands=commandString.toCharArray();
		
		if(commandString=="") {
			return "("+roverx+","+rovery+","+dir+")";
		}else {
		for(int i=0;i<commandString.length();i++) {
		
		
			if(commands[i]=='r' || commands[i]=='l') {
				if(commands[i]=='r') {
					turnRight();
					
					
				}
				if(commands[i]=='l') {
					turnLeft();
				}
				
			}else {
				if(commands[i]=='f') {
					goingForward();
					
					
				}else {
					if(commands[i]=='b') {
					goingBackward();
					
					
					}
				}
			}
		
		}
		}return "("+roverx+","+rovery+","+dir+")"+Obstacles;
		
	}

	
	public void turnRight() {
		
		switch(dir) {
		case "N":dir="E"; break;
		case "E":dir="S"; break;
		case "S":dir="W"; break;
		case "W":dir="N"; break;
		default :dir="N"; break;
		}
		
	}
	
	public void turnLeft() {
		switch(dir) {
		case "N":dir="W"; break;
		case "W":dir="S"; break;
		case "S":dir="E"; break;
		case "E":dir="N"; break;
		default :dir="W"; break;
		}
	}
	
	public void goingForward() {
		switch(dir) {
		case "N":
			if(rovery==gridMaxY-1) {
				rovery=0;
				if(planetContainsObstacleAt(roverx,rovery)) {
					addObstacles(roverx,rovery);
					rovery=gridMaxY-1;
				}
			}else {
				rovery++;
				if(planetContainsObstacleAt(roverx,rovery)) {
					addObstacles(roverx,rovery); 
					rovery--;
				}
			}
			
		break;
		
		
		case "S":
			if(rovery==0) {
				rovery=gridMaxY-1;
			}else {
				rovery--;
			}
		break;
		
		
		case "W":
			if(roverx==0) {
				roverx=gridMaxX-1;
			}else {
				roverx--;
			}
		break;
		
		case "E":
			if(roverx==gridMaxX-1) {
				roverx=0;
				if(planetContainsObstacleAt(roverx,rovery)) {
					addObstacles(roverx,rovery);
					roverx=gridMaxX-1;
				}
			}else {
				roverx++;
				if(planetContainsObstacleAt(roverx,rovery)) {
					addObstacles(roverx,rovery);
					roverx--;
				}
			}
		break;
		default :rovery++; break;
		}
	}
	
	
	public String addObstacles(int obx,int oby) {
		
	return	Obstacles="("+obx+","+oby+")";
	}

	public void goingBackward() {
		switch(dir) {
		case "N": 
			
			if(rovery==0) {
			rovery=gridMaxY-1;
			}else {
				rovery--;
			}
		break;
		
		case "S":
			if(rovery==gridMaxY-1) {
				rovery=0;
				
			}else {
				rovery++;
			}
		break;
		
		case "W":
			if(roverx==gridMaxX-1) {
				roverx=0;
			}else {
				roverx++;
			}
		break;
		
		
		case "E":
			if(roverx==0) {
				roverx=gridMaxX-1;
			}else {
				roverx--;
			}
		break;
		
		
		default :roverx--;
			 
			break;
		}
	}
}
